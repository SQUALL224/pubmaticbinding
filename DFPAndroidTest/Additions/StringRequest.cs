﻿using Java.Lang;

namespace Android.Volley.Toolbox
{
    partial class StringRequest
    {
        protected override void DeliverResponse(Object response)
        {
            DeliverResponse((String)response);
        }
    }
}