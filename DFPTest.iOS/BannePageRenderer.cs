﻿using System;
using CoreGraphics;
using DFPiOSTest;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using DFPTest;
using DFPTest.iOS;
using Foundation;
using UIKit;
using System.Diagnostics;
using DFPTest.iOS.EventHandler;

[assembly: ExportRenderer(typeof(BannerPage), typeof(BannePageRenderer))]
namespace DFPTest.iOS
{
    public class BannePageRenderer : PageRenderer
    {
        private POBBannerView bannerView;
        private string dfpAdUnit = "/15671365/pm_sdk/PMSDK-Demo-App-Banner";
        private string owAdUnit = "/15671365/pm_sdk/PMSDK-Demo-App-Banner";
        private string pubId = "156276";
        private NSNumber profileId = new NSNumber(1165);

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            UIWindow window = UIApplication.SharedApplication.Delegate.GetWindow();
            //var presentedVC = window.RootViewController;
            //while (presentedVC.PresentedViewController != null)
            //{
            //    presentedVC = presentedVC.PresentedViewController;
            //}
            try
            {
                var adSize = new CGSize(320, 50);
                var pobAdSize = new POBAdSize(adSize);

                var eventHandler = new DFPBannerEventHandler(dfpAdUnit, new CGSize[]{ adSize }, this);
                bannerView = new POBBannerView(pubId, profileId, owAdUnit, eventHandler);

                CGRect screenRect = UIScreen.MainScreen.Bounds;
                var screenWidth = screenRect.Size.Width;
                var screenHeight = screenRect.Size.Height;

                bannerView.Frame = new CGRect(0, 30, 320, 50);
                //bannerView.AutoresizingMask = UIKit.UIViewAutoresizing.FlexibleTopMargin | UIKit.UIViewAutoresizing.FlexibleLeftMargin | UIKit.UIViewAutoresizing.FlexibleRightMargin;

                bannerView.Delegate = new BannerViewDelegate((UIViewController)this);
                this.View.AddSubview(bannerView);

                bannerView.LoadAd();
                base.OnElementChanged(e);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    public class BannerViewDelegate : POBBannerViewDelegate
    {
        UIViewController presentedVC;

        public override UIViewController BannerViewPresentationController => presentedVC;

        public BannerViewDelegate(UIViewController presentedVC)
        {
            this.presentedVC = presentedVC;
        }

        public override void BannerViewDidReceiveAd(POBBannerView bannerView)
        {
            //Removed the base call here, as it crashes the app.
            //It throws - 'You_Should_Not_Call_base_In_This_Method' exception.
            Debug.WriteLine("Ad received successfully");
        }

        public override void BannerViewWillLeaveApplication(POBBannerView bannerView)
        {
            //It throws - 'You_Should_Not_Call_base_In_This_Method' exception.
            //base.BannerViewWillLeaveApplication(bannerView);
        }

        public override void BannerViewDidDismissModal(POBBannerView bannerView)
        {
            //It throws - 'You_Should_Not_Call_base_In_This_Method' exception.
            //base.BannerViewDidDismissModal(bannerView);
        }

        public override void BannerViewWillPresentModal(POBBannerView bannerView)
        {
            //It throws - 'You_Should_Not_Call_base_In_This_Method' exception.
            //base.BannerViewWillPresentModal(bannerView);
        }

        public override void BannerView(POBBannerView bannerView, NSError error)
        {
            Debug.WriteLine(error.Description.ToString());
        }
    }
}
