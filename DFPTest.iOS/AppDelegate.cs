﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreLocation;
using Foundation;
using DFPiOSTest;
using UIKit;

namespace DFPTest.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        private CLLocationManager locationManager;
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            OpenBidSDK.SetLogLevel(POBSDKLogLevel.All);
            locationManager = new CLLocationManager();
            locationManager.RequestWhenInUseAuthorization();
            OpenBidSDK.SetSSLEnabled(false);

            // Set a valid App Store URL, containing the app id of your iOS app.
            var appInfo = new POBApplicationInfo();
            appInfo.StoreURL = new NSUrl("https://itunes.apple.com/us/app/pubmatic-sdk-app/id1175273098?mt=8");
            // This application information is a global configuration & you
            // need not set this for every ad request(of any ad type)

            OpenBidSDK.SetApplicationInfo(appInfo);

            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
