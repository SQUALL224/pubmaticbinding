using System;
using System.ComponentModel;
using System.Diagnostics;
using DFPiOSTest;
using DFPTest;
using DFPTest.iOS;
using DFPTest.iOS.EventHandler;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(InterstitialPage), typeof(InterstitialPageRenderer))]
namespace DFPTest.iOS
{
    public partial class InterstitialPageRenderer : PageRenderer
    {
        private POBInterstitial interstitial;
        private string dfpAdUnit = "/15671365/pm_sdk/PMSDK-Demo-App-Interstitial";
        private string owAdUnit = "/15671365/pm_sdk/PMSDK-Demo-App-Interstitial";
        private string pubId = "156276";
        private NSNumber profileId = new NSNumber(1165);

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            UIView customView = (UIView)UINib.FromName("InterstitialPageView", null).Instantiate(this, null)[0];
            customView.Frame = this.View.Frame;
            this.View.AddSubview(customView);

            var oldPage = e.OldElement as InterstitialPage;
            if (oldPage != null)
            {
                oldPage.PropertyChanged -= HandlePagePropertyChanged;
            }

            var newPage = e.NewElement as InterstitialPage;
            if (newPage != null)
            {
                newPage.PropertyChanged += HandlePagePropertyChanged;
            }
        }

        private void HandlePagePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == InterstitialPage.ButtonTextProperty.PropertyName)
            //{
            //    UpdateButtonText();
            //}
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            DFPInterstitialEventHandler eventHandler = new DFPInterstitialEventHandler(dfpAdUnit);  

            interstitial = new POBInterstitial(pubId, profileId, owAdUnit, eventHandler);

            interstitial.Delegate = new InterstitialDelegate(this);

            BtnShowAd = btnShowAd;
        }

        partial void onLoadAd(NSObject sender)
        {
            try
            {
                this.interstitial.LoadAd();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
            }
        }

        partial void onShowAd(NSObject sender)
        {
            if (this.interstitial.IsReady)
            {
                this.interstitial.ShowFromViewController(this);
            }
        }

        public UIButton BtnShowAd
        {
            get;
            set;
        }
    }

    public class InterstitialDelegate : POBInterstitialDelegate
    {
        InterstitialPageRenderer presentedVC;

        public InterstitialDelegate(InterstitialPageRenderer presentedVC)
        {
            this.presentedVC = presentedVC;
        }

        public override void InterstitialDidReceiveAd(POBInterstitial interstitial)
        {

            presentedVC.BtnShowAd.Hidden = false;
            Debug.WriteLine("Interstitial : Ad Received");
        }

        public override void Interstitial(POBInterstitial interstitial, NSError error)
        {
            Debug.WriteLine("Interstitial : Ad failed with error : " + error.LocalizedDescription);
        }

        public override void InterstitialWillPresentAd(POBInterstitial interstitial)
        {
            Debug.WriteLine("Interstitial : Will present");
        }

        public override void InterstitialDidDismissAd(POBInterstitial interstitial)
        {
            Debug.WriteLine("Interstitial : Dismissed");
        }

        public override void InterstitialDidClickAd(POBInterstitial interstitial)
        {
            //This throws - 'You_Should_Not_Call_base_In_This_Method' exception.
            //base.InterstitialDidClickAd(interstitial);
            Debug.WriteLine("Interstitial : Ad Clicked");
        }

        public override void InterstitialWillLeaveApplication(POBInterstitial interstitial)
        {
            //This throws - 'You_Should_Not_Call_base_In_This_Method' exception.
            //base.InterstitialWillLeaveApplication(interstitial);
            Debug.WriteLine("Interstitial : Will leave app");
        }
    }
}