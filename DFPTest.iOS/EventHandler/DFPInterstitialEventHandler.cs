﻿using System;
using Google.MobileAds;
using DFPiOSTest;
using Foundation;
using UIKit;
using System.Diagnostics;

namespace DFPTest.iOS.EventHandler
{
    public class DFPInterstitialEventHandler : POBInterstitialEvent, IAppEventDelegate, IInterstitialDelegate
    {
        private readonly float SYNC_TIMEOUT_INTEREVAL = 0.8f;

        private Google.MobileAds.DoubleClick.Interstitial interstitial;
        IPOBInterstitialEventDelegate _delegate;
        private String adUnitId;
        private NSTimer timer;
        private bool notified;
        private bool isAppEventExpected;

        public DFPInterstitialEventHandler(String _adUnitId)
        {
            this.adUnitId = _adUnitId;
        }

        public override void RequestAdWithBid(POBBid bid)
        {
            notified = false;
            isAppEventExpected = false;

            // Create DFPInterstitial and set ad unit
            interstitial = new Google.MobileAds.DoubleClick.Interstitial(adUnitId);

            // Set delegates on DFPInterstitial instance, these should not be removed/overridden else event handler will not work as expected.
            interstitial.Delegate = this;
            interstitial.AppEventDelegate = this;

            // Create DFP ad request
            Google.MobileAds.DoubleClick.Request dfpRequest = new Google.MobileAds.DoubleClick.Request();

            if (interstitial.AppEventDelegate.GetType() != typeof(DFPInterstitialEventHandler) ||
                interstitial.Delegate.GetType() != typeof(DFPInterstitialEventHandler))
            {
                Debug.WriteLine("Do not set DFP delegates. These are used by DFPInterstitialEventHandler internally.");
            }

            if (bid != null)
            {
                if (bid.Status.BoolValue)
                {
                    isAppEventExpected = true;
                }

                var customTargeting = new NSMutableDictionary();
                customTargeting.AddEntries(bid.TargetingInfo);
                dfpRequest.CustomTargeting = customTargeting;
                Debug.WriteLine("Custom targeting: " + customTargeting.Description);
            }

            // Load ad request
            interstitial.LoadRequest(dfpRequest);
        }

        protected override void Dispose(bool disposing)
        {
            Debug.WriteLine("Dispose called");
            this._delegate = null;
            base.Dispose(disposing);
        }

        public override void SetDelegate(IPOBInterstitialEventDelegate @delegate)
        {
            this._delegate = @delegate;
        }

        public override void ShowFromViewController(UIViewController controller)
        {
            this.interstitial.PresentFromRootViewController(controller);
        }

        [Export("interstitial:didReceiveAppEvent:withInfo:")]
        public void InterstitialDidReceiveAppEvent(Interstitial interstitial, string name, string info)
        {
            Debug.WriteLine("isMainThread - " + NSThread.IsMain);
            if (!string.IsNullOrEmpty(name) && name == "pubmaticdm")
            {
                if (notified)
                {
                    var errorMessage = "app event is called unexpectedly";
                    var error = new NSError((Foundation.NSString)errorMessage, 0);
                    _delegate.FailedWithError(error);
                }
            }
            notified = true;
            this.interstitial.Delegate = null;
            this.interstitial = null;
            this._delegate.OpenBidPartnerDidWin();
        }

        [Export("interstitialDidReceiveAd:")]
        public void DidReceiveAd(Interstitial interstitial)
        {
            Debug.WriteLine("isMainThread - " + NSThread.IsMain);

            if (notified)
            {
                return;
            }

            // If OpenBid SDK have provided non-zero bid price, expect for app event for fixed time interval, otherwise consider as DFP has won & serving its own ad
            if (!isAppEventExpected) {
                if (!notified)
                {
                    this._delegate.AdServerDidWin();
                    notified = true;
                }
            }
            else
            {
                if (timer != null)
                {
                    timer.Invalidate();
                }
                timer = NSTimer.CreateScheduledTimer(SYNC_TIMEOUT_INTEREVAL, (obj) => { 
                    if (!notified)
                    {
                        this._delegate.AdServerDidWin();
                        notified = true;
                    }
                });
            }
        }

        [Export("interstitial:didFailToReceiveAdWithError:")]
        public void DidFailToReceiveAd(Interstitial interstitial, RequestError requestError)
        {
            NSError eventError;
            switch (requestError.Code) {
                case (int)ErrorCode.NoFill:
                    eventError = new NSError(Constants.kPOBErrorDomain, (int)POBErrorCode.ErrorNoAds, requestError.UserInfo);
                    break;
                case (int)ErrorCode.InvalidRequest:
                    eventError = new NSError(Constants.kPOBErrorDomain, (int)POBErrorCode.ErrorInvalidRequest, requestError.UserInfo);
                    break;
                case (int)ErrorCode.NetworkError:
                    eventError = new NSError(Constants.kPOBErrorDomain, (int)POBErrorCode.ErrorNetworkError, requestError.UserInfo);
                    break;
                case (int)ErrorCode.Timeout:
                    eventError = new NSError(Constants.kPOBErrorDomain, (int)POBErrorCode.ErrorTimeout, requestError.UserInfo);
                    break;
                case (int)ErrorCode.InternalError:
                case (int)ErrorCode.InterstitialAlreadyUsed:
                case (int)ErrorCode.MediationDataError:
                case (int)ErrorCode.MediationAdapterError:
                case (int)ErrorCode.MediationNoFill:
                case (int)ErrorCode.MediationInvalidAdSize:
                case (int)ErrorCode.InvalidArgument:
                    eventError = new NSError(Constants.kPOBErrorDomain, (int)POBErrorCode.ErrorInternalError, requestError.UserInfo);
                    break;
                case (int)ErrorCode.ReceivedInvalidResponse:
                    eventError = new NSError(Constants.kPOBErrorDomain, (int)POBErrorCode.ErrorInvalidResponse, requestError.UserInfo);
                    break;
                default:
                    eventError = requestError;
                    break;
            }

            this._delegate.FailedWithError(eventError);
        }

        [Export("interstitialWillPresentScreen:")]
        public void WillPresentScreen(Interstitial interstitial)
        {
            this._delegate.WillPresentAd();
        }

        [Export("interstitialDidDismissScreen:")]
        public void DidDismissScreen(Interstitial interstitial)
        {
            this._delegate.DidDismissAd();
        }

        [Export("interstitialWillLeaveApplication:")]
        public void WillLeaveApplication(Interstitial interstitial)
        {
            this._delegate.DidClickAd();
            this._delegate.WillLeaveApp();
        }
    }
}
