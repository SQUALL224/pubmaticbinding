﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CoreGraphics;
using Foundation;
using DFPiOSTest;
using UIKit;
using Google.MobileAds;

namespace DFPTest.iOS.EventHandler
{
    public class DFPBannerEventHandler : POBBannerEvent, IBannerViewDelegate, IAppEventDelegate, IAdSizeDelegate
    {
        private readonly float SYNC_TIMEOUT_INTERVAL = 0.6f;

        private Google.MobileAds.DoubleClick.BannerView bannerView;
        private NSTimer timer;
        public IPOBBannerEventDelegate _delegate;
        private CGSize dfpAdSize = CGSize.Empty;
        private bool notified = false;
        private bool isAppEventExpected = false;
        private CGSize[] adSizes = new CGSize[] { };
        [Weak] UIViewController rootVC;

        public DFPBannerEventHandler(String adUnitId, CGSize[] validSizes, UIViewController _rootVC)
        {
            bannerView = new Google.MobileAds.DoubleClick.BannerView();
            bannerView.AdUnitID = adUnitId;

            adSizes = validSizes;
            var adSize = new CGSize(320, 50);

            NSValue sizeVal = NSValue.FromCGSize(adSize);
            bannerView.ValidAdSizes = new NSValue[] { sizeVal };
            bannerView.Frame = new CGRect(0, 0, 320, 50);
            dfpAdSize = bannerView.Frame.Size;

            bannerView.Delegate = this;
            bannerView.AppEventDelegate = this;
            bannerView.AdSizeDelegate = this;

            rootVC = _rootVC;
        }

        public override CGSize AdContentSize => dfpAdSize;

        public override POBAdSize[] RequestedAdSizes => RequestedSizes();

        private POBAdSize[] RequestedSizes()
        {
            var sizes = new List<POBAdSize>();
            foreach (var size in adSizes)
            {
                sizes.Add(new POBAdSize(size));
            }
            return sizes.ToArray();
        }

        public override void RequestAdWithBid(POBBid bid)
        {
            notified = false;
            isAppEventExpected = false;
            bannerView.RootViewController = _delegate.ViewControllerForPresentingModal;
            var dfpRequest = new Google.MobileAds.DoubleClick.Request();

            if (bannerView.AppEventDelegate.GetType() != typeof(DFPBannerEventHandler) ||
                bannerView.Delegate.GetType() != typeof(DFPBannerEventHandler)) {
                Debug.WriteLine("Do not set DFP delegates. These are used by DFPBannerEventHandler internally.");
            }

            if (bid != null)
            {
                if (bid.Status.BoolValue == false)
                {
                    isAppEventExpected = true;
                }

                var customTargeting = new NSMutableDictionary();
                customTargeting.AddEntries(bid.TargetingInfo ?? new NSDictionary());
                dfpRequest.CustomTargeting = customTargeting;
                Debug.WriteLine("Custom targeting: " + customTargeting.Description);
            }

            bannerView.LoadRequest(dfpRequest);
        }

        public override void SetDelegate(IPOBBannerEventDelegate _delegate)
        {
            this._delegate = _delegate;
        }

        [Export("adView:didReceiveAppEvent:withInfo:")]
        public void AdView(Google.MobileAds.BannerView banner, string name, string info)
        {
            if (name == "pubmaticdm")
            {
                if (notified)
                {
                    var errorMessage = "app event is called unexpectedly";
                    var error = new NSError((Foundation.NSString)errorMessage, 0);
                    _delegate.FailedWithError(error);
                }
                Debug.WriteLine("OpenBidPartnerDidWin");

                notified = true;
                _delegate.OpenBidPartnerDidWin();
            }
        }

        [Export("adViewDidReceiveAd:")]
        public void AdViewDidReceiveAd(Google.MobileAds.BannerView bannerView)
        {
            Debug.WriteLine("adViewDidReceiveAd:");

            if (notified)
            {
                return;
            }

            if (!isAppEventExpected)
            {
                _delegate.AdServerDidWin((UIView)bannerView);
                notified = true;
            } else
            {
                timer.Invalidate();
                timer = NSTimer.CreateScheduledTimer(SYNC_TIMEOUT_INTERVAL, (obj) => { 
                    if (!notified)
                    {
                        _delegate.AdServerDidWin((UIView)bannerView);
                        notified = true;
                    }
                });
            }
        }

        [Export("adView:didFailToReceiveAdWithError:")]
        public void AdView(NSObject banner, NSError error)
        {
            Console.WriteLine("banner failed \n "+error.LocalizedDescription);
            _delegate.FailedWithError(error);
        }

        [Export("adViewWillPresentScreen:")]
        public void AdViewWillPresentScreen(Google.MobileAds.BannerView bannerView)
        {
            _delegate.WillPresentModal();
        }

        [Export("adViewDidDismissScreen:")]
        public void AdViewDidDismissScreen(Google.MobileAds.BannerView bannerView)
        {
            _delegate.DidDismissModal();
        }

        [Export("adViewWillLeaveApplication:")]
        public void AdViewWillLeaveApplication(Google.MobileAds.BannerView bannerView)
        {
            _delegate.WillLeaveApp();
        }

        public void WillChangeAdSizeTo(Google.MobileAds.BannerView view, AdSize size)
        {
            Console.WriteLine(size.ToString());
        }
    }
}
