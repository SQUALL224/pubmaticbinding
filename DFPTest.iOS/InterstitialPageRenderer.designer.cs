// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace DFPTest.iOS
{
    [Register ("InterstitialPageRenderer")]
    partial class InterstitialPageRenderer
    {
        [Outlet]
        UIKit.UIButton btnLoadAd { get; set; }


        [Outlet]
        UIKit.UIButton btnShowAd { get; set; }


        [Action ("onLoadAd:")]
        partial void onLoadAd (Foundation.NSObject sender);


        [Action ("onShowAd:")]
        partial void onShowAd (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
        }
    }
}