﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Ads;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Pubmatic.Sdk.Common;
using Com.Pubmatic.Sdk.Common.Models;
using Com.Pubmatic.Sdk.Openbid.Banner;
using DFPTest;
using DFPTest.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BannerPage), typeof(BannerPageRenderer))]
namespace DFPTest.Droid
{
    public class BannerPageRenderer : PageRenderer
    {
        Activity activity;

        private static String OPENWRAP_AD_UNIT_ID = "/15671365/pm_sdk/PMSDK-Demo-App-Banner";
        private static String PUB_ID = "156276";
        private static int PROFILE_ID = 1165;
        private static String DFP_AD_UNIT_ID = "/15671365/pm_sdk/PMSDK-Demo-App-Banner";

        private POBBannerView banner;

        public BannerPageRenderer() : base(null)
        {
            throw new Exception("This constructor should never be used");
        }

        public BannerPageRenderer(Context context) : base(context)
        {
            activity = context as Activity;
            activity.SetContentView(Resource.Layout.BannerPageLayout);
            //view = LayoutInflater.From(this.Context).Inflate(Resource.Layout.BannerPageLayout, null, false);

            POBApplicationInfo appInfo = new POBApplicationInfo();
            try
            {
                appInfo.StoreURL = new Java.Net.URL("https://play.google.com/store/apps/details?id=com.example.android&hl=en");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // This app information is a global configuration & you
            // Need not set this for every ad request(of any ad type)
            OpenBidSDK.SetApplicationInfo(appInfo);
            // Create a banner custom event handler for your ad server. Make sure you use
            // separate event handler objects to create each banner view.
            // For example, The code below creates an event handler for DFP ad server.
            DFPBannerEventHandler eventHandler = new DFPBannerEventHandler(context, DFP_AD_UNIT_ID, AdSize.Banner);

            // Initialise banner view
            banner = (Com.Pubmatic.Sdk.Openbid.Banner.POBBannerView)activity.FindViewById(Resource.Id.banner);
            banner.Init(PUB_ID, PROFILE_ID, OPENWRAP_AD_UNIT_ID, eventHandler);

            //optional listener to listen banner events
            banner.SetListener(new POBBannerView.POBBannerViewListener());

            // Call loadAd() on banner instance
            banner.LoadAd();
        }
    }
}
