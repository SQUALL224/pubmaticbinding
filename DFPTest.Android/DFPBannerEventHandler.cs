﻿using System;
using System.Collections.Generic;
using System.Timers;
using Android.Content;
using Android.Gms.Ads;
using Android.Gms.Ads.DoubleClick;
using Com.Pubmatic.Sdk.Common;
using Com.Pubmatic.Sdk.Openbid.Banner;
using Com.Pubmatic.Sdk.Openbid.Core;
using Com.Pubmatic.Sdk.Webrendering.UI;

namespace DFPTest.Droid
{
    public class DFPBannerEventHandler : AdListener, IPOBBannerEvent, IAppEventListener
    {
        private static String PUBMATIC_WIN_KEY = "pubmaticdm";

        private DFPConfigListener dfpConfigListener;

        private bool? notifiedBidWin;

        private Boolean isAppEventExpected;

        private Timer timer;

        private PublisherAdView dfpAdView;

        private IPOBBannerEventListener eventListener;


        public DFPBannerEventHandler(Context context, String adUnitId, AdSize adSize)
        {
            dfpAdView = new PublisherAdView(context.ApplicationContext);
            dfpAdView.AdUnitId = adUnitId;
            dfpAdView.SetAdSizes(adSize);

            dfpAdView.AdListener = this;
            dfpAdView.AppEventListener = this;
        }

        public void SetConfigListener(DFPConfigListener listener)
        {
            dfpConfigListener = listener;
        }

        private void ResetDelay()
        {
            if (timer != null)
            {
                timer.Stop();
            }
            timer = null;
        }

        private void ScheduleDelay()
        {
            ResetDelay();

            timer = new Timer(400);
            timer.Elapsed += (sender, e) =>
            {
                notifyPOBAboutAdReceived();
            };
        }

        private void notifyPOBAboutAdReceived()
        {
            if (notifiedBidWin == null)
            {
                notifiedBidWin = false;
                if (eventListener != null)
                {
                    eventListener.OnAdServerWin(dfpAdView);
                }
            }
        }

        private void SendErrorToPOB(POBError error)
        {
            if (eventListener != null && error != null)
            {
                eventListener.OnFailed(error);
            }
        }

        public POBAdSize AdSize => (dfpAdView.AdSize != null) ? new POBAdSize(dfpAdView.AdSize.Width, dfpAdView.AdSize.Height) : null; 

        public void Destroy()
        {
            ResetDelay();
            if (dfpAdView != null)
            {
                dfpAdView.Destroy();
            }
            dfpAdView = null;
            eventListener = null;
        }

        public void OnAppEvent(string name, string data)
        {
            if (name.Equals(PUBMATIC_WIN_KEY))
            {
                if (notifiedBidWin == null)
                {
                    notifiedBidWin = true;
                    eventListener.OnOpenBidPartnerWin();
                }
                else if (notifiedBidWin == false)
                {
                    SendErrorToPOB(new POBError(POBError.OpenBidSignalingError, "DFP ad server mismatched bid win signal"));
                }
            }
        }

        public void RequestAd(POBBid bid)
        {
            isAppEventExpected = false;

            PublisherAdRequest.Builder requestBuilder = new PublisherAdRequest.Builder();

            // Check if publisher want to set any targeting data
            if (dfpConfigListener != null)
            {
                dfpConfigListener.configure(dfpAdView, requestBuilder);
            }

            // Warn publisher if he overrides the DFP listeners
            if (dfpAdView.AdListener != this || dfpAdView.AppEventListener != this)
            {
                Console.WriteLine("Do not set DFP listeners. These are used by DFPBannerEventHandler internally.");
            }

            if (bid != null)
            {

                // Logging details of bid objects for debug purpose.
                Console.WriteLine(bid.ToString());

                Dictionary<string, string> targeting = new Dictionary<string, string>();
                foreach (var key in bid.TargetingInfo.Keys)
                {
                    targeting.Add(key, bid.TargetingInfo[key]);
                }

                if (targeting != null && targeting.Count != 0)
                {
                    // using for-each loop for iteration over Map.entrySet()
                    foreach (var key1 in targeting.Keys)
                    {
                        requestBuilder.AddCustomTargeting(key1, targeting[key1]);
                        Console.WriteLine("Targeting param [" + key1 + "] = " + targeting[key1]);
                    }
                }

                // Save this flag for future reference. It will be referred to wait for onAppEvent, only
                // if POB delivers non-zero bid to DFP SDK.
                double price = bid.Price;
                if (price > 0.0d)
                {
                    isAppEventExpected = true;
                }
            }

            PublisherAdRequest adRequest = requestBuilder.Build();

            // Publisher/App developer can add extra targeting parameters to dfpAdView here.
            notifiedBidWin = null;

            // Load DFP ad request
            dfpAdView.LoadAd(adRequest);
        }

        public POBAdSize[] RequestedAdSizes()
        {
            POBAdSize[] adSizes = null;
            if (dfpAdView != null)
            {
                AdSize[] dfpAdSizes = dfpAdView.GetAdSizes();
                if (dfpAdSizes != null && dfpAdSizes.Length  > 0)
                {
                    adSizes = new POBAdSize[dfpAdSizes.Length];
                    for (int index = 0; index < dfpAdSizes.Length; index++)
                    {
                        adSizes[index] = new POBAdSize(dfpAdSizes[index].Width, dfpAdSizes[index].Height);
                    }
                }
            }
            return adSizes;
        }

        public void SetEventListener(IPOBBannerEventListener listener)
        {
            eventListener = listener;
        }

        public IPOBBannerRendering GetRenderer(string p0)
        {
            return null;
        }

        public override void OnAdFailedToLoad(int errorCode)
        {
            if (eventListener != null)
            {
                switch (errorCode)
                {
                    case PublisherAdRequest.ErrorCodeInvalidRequest:
                        eventListener.OnFailed(new POBError(POBError.InvalidRequest, "DFP SDK gives invalid request error"));
                        break;
                    case PublisherAdRequest.ErrorCodeNetworkError:
                        eventListener.OnFailed(new POBError(POBError.NetworkError, "DFP SDK gives network error"));
                        break;
                    case PublisherAdRequest.ErrorCodeNoFill:
                        eventListener.OnFailed(new POBError(POBError.NoAdsAvailable, "DFP SDK gives no fill error"));
                        break;
                    default:
                        eventListener.OnFailed(new POBError(POBError.InternalError, "DFP SDK failed with error code:" + errorCode));
                        break;
                }
            }
            else
            {
                Console.WriteLine("Can not call failure callback, POBBannerEventListener reference null. DFP error:" + errorCode);
            }
        }

        public override void OnAdOpened()
        {
            if (eventListener != null)
            {
                eventListener.OnAdOpened();
            }
        }

        public override void OnAdClosed()
        {
            if (eventListener != null)
            {
                eventListener.OnAdClosed();
            }
        }

        public override void OnAdLoaded()
        {
            if (eventListener != null)
            {
                if (notifiedBidWin == null)
                {
                    if (isAppEventExpected)
                    {
                        ScheduleDelay();
                    }
                    else
                    {
                        notifyPOBAboutAdReceived();
                    }
                }
            }
        }

        public override void OnAdLeftApplication()
        {
            base.OnAdLeftApplication();
            if (eventListener != null)
            {
                eventListener.OnAdLeftApplication();
            }
        }

        public interface DFPConfigListener
        {
            void configure(PublisherAdView adView, PublisherAdRequest.Builder requestBuilder);
        }
    }
}
