﻿using System;
using System.Collections.Generic;
using System.Timers;
using Android.Content;
using Android.Gms.Ads;
using Android.Gms.Ads.DoubleClick;
using Com.Pubmatic.Sdk.Common;
using Com.Pubmatic.Sdk.Openbid.Core;
using Com.Pubmatic.Sdk.Openbid.Interstitial;
using Com.Pubmatic.Sdk.Webrendering.UI;
using static DFPTest.Droid.DFPBannerEventHandler;

namespace DFPTest.Droid
{
    public class DFPInterstitialEventHandler : AdListener, IPOBInterstitialEvent, IAppEventListener
    {
        private static String PUBMATIC_WIN_KEY = "pubmaticdm";
   
        private DFPConfigListener dfpConfigListener;
        /*
         * Flag to identify if PubMatic bid wins the current impression
         */
        private bool? notifiedBidWin;

        private bool isAppEventExpected;
        /*
         * Timer object to synchronize the onAppEvent() of DFP SDK with onAdLoaded()
         */
        private Timer timer;
        /*
         *
         */
        private Context context;
        /**
         *
         */
        private String adUnitId;
        /**
         * DFP Interstitial ad
         */
        private PublisherInterstitialAd dfpInterstitialAd;
        /**
         * Interface to pass the DFP ad event to OpenBid SDK
         */
        private IPOBInterstitialEventListener eventListener;

        public DFPInterstitialEventHandler(Context context, String adUnitId)
        {
            this.context = context;
            this.adUnitId = adUnitId;
        }


        public void SetConfigListener(DFPConfigListener listener)
        {
            dfpConfigListener = listener;
        }

        private void initializeDFView()
        {
            if (dfpInterstitialAd != null)
            {
                dfpInterstitialAd = null;
            }
            dfpInterstitialAd = new PublisherInterstitialAd(context.ApplicationContext);
            dfpInterstitialAd.AdUnitId = adUnitId;

            // DO NOT REMOVE/OVERRIDE BELOW LISTENERS
            dfpInterstitialAd.AdListener = this;
            dfpInterstitialAd.AppEventListener = this;
        }

        private void ResetDelay()
        {
            if (timer != null)
            {
                timer.Stop();
            }
            timer = null;
        }

        private void ScheduleDelay()
        {
            ResetDelay();

            timer = new Timer(400);
            timer.Elapsed += (sender, e) =>
            {
                NotifyPOBAboutAdReceived();
            };
        }

        private void NotifyPOBAboutAdReceived()
        {
            // If onAppEvent is not called within 400 milli-sec, consider that DFP wins
            if (notifiedBidWin == null)
            {
                // Notify POB SDK about DFP ad win state and set the state
                notifiedBidWin = false;
                if (eventListener != null)
                {
                    eventListener.OnAdServerWin();
                }
            }
        }

        private void SendErrorToPOB(POBError error)
        {
            if (eventListener != null && error != null)
            {
                eventListener.OnFailed(error);
            }
        }

        public IPOBInterstitialRendering GetRenderer(String partnerName)
        {
            return null;
        }

        public void Destroy()
        {
            //Do Final cleaup
            ResetDelay();
            dfpInterstitialAd = null;
            dfpConfigListener = null;
            eventListener = null;
            context = null;
        }

        public void OnAppEvent(string name, string data)
        {
            Console.WriteLine("DFPInterstitialEventHandler------OnAppEvent");

            if (name.Equals(PUBMATIC_WIN_KEY))
            {
                // If onAppEvent is called before onAdLoaded(), it means POB bid wins
                if (notifiedBidWin == null)
                {
                    notifiedBidWin = true;
                    eventListener.OnOpenBidPartnerWin();
                }
                else if (notifiedBidWin != true)
                {
                    // In this case onAppEvent is called in wrong order and within 400 milli-sec
                    // Hence, notify POB SDK about DFP ad win state
                    SendErrorToPOB(new POBError(POBError.OpenBidSignalingError, "DFP ad server mismatched bid win signal"));
                }
            }
        }

        public void RequestAd(POBBid bid)
        {
            isAppEventExpected = false;

            PublisherAdRequest.Builder requestBuilder = new PublisherAdRequest.Builder();

            initializeDFView();

            // Check if publisher want to set any targeting data
            if (dfpConfigListener != null)
            {
                dfpConfigListener.configure(dfpInterstitialAd, requestBuilder);
            }

            // Warn publisher if he overrides the DFP listeners
            if (dfpInterstitialAd.AdListener != this || dfpInterstitialAd.AppEventListener != this)
            {
                Console.WriteLine("Do not set DFP listeners. These are used by DFPInterstitialEventHandler internally.");
            }

            if (bid != null)
            {

                // Logging details of bid objects for debug purpose.
                Dictionary<string, string> targeting = new Dictionary<string, string>();
                foreach (var key in bid.TargetingInfo.Keys)
                {
                    targeting.Add(key, bid.TargetingInfo[key]);
                }
                if (targeting != null && targeting.Count != 0)
                {
                    // using for-each loop for iteration over Map.entrySet()
                    foreach (var key1 in targeting.Keys)
                    {
                        requestBuilder.AddCustomTargeting(key1, targeting[key1]);
                        Console.WriteLine("Targeting param [" + key1 + "] = " + targeting[key1]);
                    }
                }

                // Save this flag for future reference. It will be referred to wait for onAppEvent, only
                // if POB delivers non-zero bid to DFP SDK.
                double price = bid.Price;
                if (price > 0.0d)
                {
                    isAppEventExpected = true;
                }
            }

            PublisherAdRequest adRequest = requestBuilder.Build();

            // Publisher/App developer can add extra targeting parameters to dfpInterstitialAd here.
            notifiedBidWin = null;

            // Load DFP ad request
            dfpInterstitialAd.LoadAd(adRequest);
        }

        public void SetEventListener(IPOBInterstitialEventListener listener)
        {
            eventListener = listener;
        }

        public void Show()
        {
            if (null != dfpInterstitialAd && dfpInterstitialAd.IsLoaded)
            {
                dfpInterstitialAd.Show();
            }
            else
            {
                String errMsg = "DFP SDK is not ready to show Interstitial Ad.";
                if (null != eventListener)
                {
                    SendErrorToPOB(new POBError(POBError.InterstitialNotReady, errMsg));
                }
            }
        }

        public override void OnAdFailedToLoad(int errCode)
        {
            Console.WriteLine("InterstitialEventHandler--------OnAdFailedToLoad");
            if (eventListener != null)
            {
                switch (errCode)
                {
                    case PublisherAdRequest.ErrorCodeInvalidRequest:
                        eventListener.OnFailed(new POBError(POBError.InvalidRequest, "DFP SDK gives invalid request error"));
                        break;
                    case PublisherAdRequest.ErrorCodeNetworkError:
                        eventListener.OnFailed(new POBError(POBError.NetworkError, "DFP SDK gives network error"));
                        break;
                    case PublisherAdRequest.ErrorCodeNoFill:
                        eventListener.OnFailed(new POBError(POBError.NoAdsAvailable, "DFP SDK gives no fill error"));
                        break;
                    default:
                        eventListener.OnFailed(new POBError(POBError.InternalError, "DFP SDK failed with error code:" + errCode));
                        break;
                }
            }
            else
            {
                Console.WriteLine("Can not call failure callback, POBInterstitialEventListener reference null. DFP error:" + errCode);
            }
        }

        public override void OnAdOpened()
        {
            if (eventListener != null)
            {
                eventListener.OnAdOpened();
            }
        }

        public override void OnAdClosed()
        {
            if (eventListener != null)
            {
                eventListener.OnAdClosed();
            }
        }

        public override void OnAdLoaded()
        {
            if (eventListener != null)
            {
                if (notifiedBidWin == null)
                {

                    // Check if POB bid delivers non-zero bids to DFP, then only wait
                    if (isAppEventExpected)
                    {
                        // Wait for 400 milli-sec to get onAppEvent before conveying to POB SDK
                        ScheduleDelay();
                    }
                    else
                    {
                        NotifyPOBAboutAdReceived();
                    }
                }
            }
        }

        public override void OnAdLeftApplication()
        {
            base.OnAdLeftApplication();
            if (eventListener != null)
            {
                eventListener.OnAdClick();
                eventListener.OnAdLeftApplication();
            }
        }

        public interface DFPConfigListener
        {
            void configure(PublisherInterstitialAd adView, PublisherAdRequest.Builder requestBuilder);
        }
    }
}
