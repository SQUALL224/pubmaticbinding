﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Ads;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Pubmatic.Sdk.Common;
using Com.Pubmatic.Sdk.Common.Models;
using Com.Pubmatic.Sdk.Openbid.Banner;
using Com.Pubmatic.Sdk.Openbid.Interstitial;
using DFPTest;
using DFPTest.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Com.Pubmatic.Sdk.Openbid.Interstitial.POBInterstitial;

[assembly: ExportRenderer(typeof(InterstitialPage), typeof(InterstitialPageRenderer))]
namespace DFPTest.Droid
{
    public class InterstitialPageRenderer : PageRenderer
    {
        Activity activity;
        public Android.Widget.Button showAdBtn, loadAdBtn;

        private static String OPENWRAP_AD_UNIT_ONE = "/15671365/pm_sdk/PMSDK-Demo-App-Interstitial";
        private static String PUB_ID = "156276";
        private static int PROFILE_ID = 1165;
        private static String DFP_AD_UNIT_ID = "/15671365/pm_sdk/PMSDK-Demo-App-Interstitial";

        private POBInterstitial interstitial;

        public InterstitialPageRenderer() : base(null)
        {
            throw new Exception("This constructor should never be used");
        }

        public InterstitialPageRenderer(Context context) : base(context)
        {
            activity = context as Activity;
            activity.SetContentView(Resource.Layout.InterstitialPageLayout);
            //view = LayoutInflater.From(this.Context).Inflate(Resource.Layout.BannerPageLayout, null, false);

            POBApplicationInfo appInfo = new POBApplicationInfo();
            try
            {
                appInfo.StoreURL = new Java.Net.URL("https://play.google.com/store/apps/details?id=com.example.android&hl=en");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // This app information is a global configuration & you
            // Need not set this for every ad request(of any ad type)
            OpenBidSDK.SetApplicationInfo(appInfo);
            // Create a banner custom event handler for your ad server. Make sure you use
            // separate event handler objects to create each banner view.
            // For example, The code below creates an event handler for DFP ad server.
            DFPInterstitialEventHandler eventHandler = new DFPInterstitialEventHandler(context, DFP_AD_UNIT_ID);

            // Initialise banner view
            interstitial = new POBInterstitial(context, PUB_ID, PROFILE_ID, OPENWRAP_AD_UNIT_ONE, eventHandler);

            CustomPOBInterstitialListener listener = new CustomPOBInterstitialListener(this);

            interstitial.SetListener(listener);

            ((Android.Widget.Button)activity.FindViewById(Resource.Id.showAdBtn)).Click += (sender, e) =>
            {
                showInterstitialAd();
            };

            ((Android.Widget.Button)activity.FindViewById(Resource.Id.loadAdBtn)).Click += (sender, e) =>
            {
                ((Android.Widget.Button)activity.FindViewById(Resource.Id.showAdBtn)).Enabled = false;
                interstitial.LoadAd();
            };
        }

        private void showInterstitialAd()
        {
            if (interstitial != null && interstitial.IsReady)
            {
                interstitial.Show();
            }
        }

        private class CustomPOBInterstitialListener : POBInterstitialListener
        {
            InterstitialPageRenderer _parent;

            public CustomPOBInterstitialListener(InterstitialPageRenderer parent)
            {
                _parent = parent;
            }

            public override void OnAdReceived(POBInterstitial ad)
            {
                Console.WriteLine("POBInterstitialListener----------onAdReceived");
                _parent.activity.FindViewById(Resource.Id.showAdBtn).Enabled = true;
            }

            public override void OnAdFailed(POBInterstitial ad, POBError error)
            {
                Console.WriteLine("POBInterstitialListener----------OnAdFailed");
            }

            public override void OnAppLeaving(POBInterstitial ad)
            {
                Console.WriteLine("POBInterstitialListener----------OnAppLeaving");
            }

            public override void OnAdOpened(POBInterstitial ad)
            {
                Console.WriteLine("POBInterstitialListener----------OnAdOpened");
            }

            public override void OnAdClosed(POBInterstitial ad)
            {
                Console.WriteLine("POBInterstitialListener----------OnAdClosed");
            }

            public override void OnAdClicked(POBInterstitial ad)
            {
                Console.WriteLine("POBInterstitialListener----------OnAdClicked");
            }
        }
    }
}
