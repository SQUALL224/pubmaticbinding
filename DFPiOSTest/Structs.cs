﻿using System;
using System.Runtime.InteropServices;
using CoreGraphics;
using ObjCRuntime;

namespace DFPiOSTest
{
    [Native]
    public enum POBErrorCode : long
    {
        ErrorInvalidRequest = 1001,
        ErrorNoAds,
        ErrorNetworkError,
        ErrorServerError,
        ErrorTimeout,
        ErrorInternalError,
        ErrorInvalidResponse,
        ErrorRequestCancelled,
        ErrorRenderError,
        SignalingError,
        ErrorInterstitialAlreadyShown = 2001,
        ErrorInterstitialNotReady = 2002
    }

    [Native]
    public enum POBHashType : long
    {
        Raw = 1,
        Sha1,
        Md5
    }

    [Native]
    public enum POBGender : long
    {
        Other = 0,
        Male,
        Female
    }

    static class CFunctions
    {
        // extern POBAdSize * POBAdSizeMakeFromCGSize (CGSize size);
        [DllImport("__Internal")]
        static extern POBAdSize POBAdSizeMakeFromCGSize(CGSize size);

        // extern POBAdSize * POBAdSizeMake (CGFloat width, CGFloat height);
        [DllImport("__Internal")]
        static extern POBAdSize POBAdSizeMake(nfloat width, nfloat height);
    }

    [Native]
    public enum POBSDKLogLevel : ulong
    {
        Off = 0,
        Error,
        Warning,
        Info,
        Debug,
        Verbose,
        All
    }

    [Native]
    public enum POBLocSource : long
    {
        Gps = 1,
        IPAddress,
        UserProvided
    }

    [Native]
    public enum POBAdPosition : long
    {
        UnKnown,
        AboveFold,
        BelowFold = 3,
        Header,
        Footer,
        Sidebar,
        Fullscreen
    }
}

