﻿using System;

using ObjCRuntime;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreLocation;

namespace DFPiOSTest
{
    [Static]
    partial interface Constants
    {
        // extern NSString *const kPOBErrorDomain;
        [Field("kPOBErrorDomain", "__Internal")]
        NSString kPOBErrorDomain { get; }

        // extern const POBBOOL POBBOOLYes;
        [Field("POBBOOLYes", "__Internal")]
        nint POBBOOLYes { get; }

        // extern const POBBOOL POBBOOLNo;
        [Field("POBBOOLNo", "__Internal")]
        nint POBBOOLNo { get; }

        // extern NSString *const kWDealIdKey;
        [Field("kWDealIdKey", "__Internal")]
        NSString kWDealIdKey { get; }

        // extern NSString *const kBidIdKey;
        [Field("kBidIdKey", "__Internal")]
        NSString kBidIdKey { get; }

        // extern NSString *const kBidPriceKey;
        [Field("kBidPriceKey", "__Internal")]
        NSString kBidPriceKey { get; }

        // extern NSString *const kBidStatusKey;
        [Field("kBidStatusKey", "__Internal")]
        NSString kBidStatusKey { get; }

        // extern NSString *const kBidPartnerKey;
        [Field("kBidPartnerKey", "__Internal")]
        NSString kBidPartnerKey { get; }

        // extern NSString *const kBidSizeKey;
        [Field("kBidSizeKey", "__Internal")]
        NSString kBidSizeKey { get; }

        // extern NSString *const kWrapperPlatformKey;
        [Field("kWrapperPlatformKey", "__Internal")]
        NSString kWrapperPlatformKey { get; }

        // extern NSString *const kRTBBannerParam;
        [Field("kRTBBannerParam", "__Internal")]
        NSString kRTBBannerParam { get; }

        // extern NSString *const kPositionParam;
        [Field("kPositionParam", "__Internal")]
        NSString kPositionParam { get; }

        // extern NSString *const kRTBInstlParam;
        [Field("kRTBInstlParam", "__Internal")]
        NSString kRTBInstlParam { get; }

        // extern NSString *const kRTBFormatParam;
        [Field("kRTBFormatParam", "__Internal")]
        NSString kRTBFormatParam { get; }

        // extern NSString *const kPMWAdWidth;
        [Field("kPMWAdWidth", "__Internal")]
        NSString kPMWAdWidth { get; }

        // extern NSString *const kPMWAdHeight;
        [Field("kPMWAdHeight", "__Internal")]
        NSString kPMWAdHeight { get; }
    }

    // @protocol POBAdDescriptor
    /*
      Check whether adding [Model] to this declaration is appropriate.
      [Model] is used to generate a C# class that implements this protocol,
      and might be useful for protocols that consumers are supposed to implement,
      since consumers can subclass the generated class instead of implementing
      the generated interface. If consumers are not supposed to implement this
      protocol, then [Model] is redundant and will generate code that will never
      be used.
    */
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBAdDescriptor
    {
        // @required -(NSString *)renderableContent;
        [Abstract]
        [Export("renderableContent")]
        string RenderableContent { get; }

        // @required -(CGSize)contentSize;
        [Abstract]
        [Export("contentSize")]
        CGSize ContentSize { get; }
    }

    // @protocol POBAdRendererDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBAdRendererDelegate
    {
        // @required -(void)rendererDidRenderAd:(id)renderedAd forDescriptor:(id<POBAdDescriptor>)ad;
        [Abstract]
        [Export("rendererDidRenderAd:forDescriptor:")]
        void RendererDidRenderAd(NSObject renderedAd, POBAdDescriptor ad);

        // @required -(void)rendererDidFailToRenderAdWithError:(NSError *)error forDescriptor:(id<POBAdDescriptor>)ad;
        [Abstract]
        [Export("rendererDidFailToRenderAdWithError:forDescriptor:")]
        void RendererDidFailToRenderAdWithError(NSError error, POBAdDescriptor ad);

        // @required -(void)rendererWillLeaveApp;
        [Abstract]
        [Export("rendererWillLeaveApp")]
        void RendererWillLeaveApp();

        // @required -(void)rendererWillPresentModal;
        [Abstract]
        [Export("rendererWillPresentModal")]
        void RendererWillPresentModal();

        // @required -(void)rendererDidDismissModal;
        [Abstract]
        [Export("rendererDidDismissModal")]
        void RendererDidDismissModal();

        // @required -(UIViewController *)viewControllerForPresentingModal;
        [Abstract]
        [Export("viewControllerForPresentingModal")]
        UIViewController ViewControllerForPresentingModal { get; }

        // @optional -(void)rendererDidUnloadMRAIDAd;
        [Export("rendererDidUnloadMRAIDAd")]
        void RendererDidUnloadMRAIDAd();

        // @optional -(void)rendererDidClickAdForDescriptor:(id<POBAdDescriptor>)ad;
        [Export("rendererDidClickAdForDescriptor:")]
        void RendererDidClickAdForDescriptor(POBAdDescriptor ad);
    }

    // @protocol POBBannerRendering
    /*
      Check whether adding [Model] to this declaration is appropriate.
      [Model] is used to generate a C# class that implements this protocol,
      and might be useful for protocols that consumers are supposed to implement,
      since consumers can subclass the generated class instead of implementing
      the generated interface. If consumers are not supposed to implement this
      protocol, then [Model] is redundant and will generate code that will never
      be used.
    */
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBBannerRendering
    {
        // @required -(void)setDelegate:(id<POBAdRendererDelegate>)delegate;
        [Abstract]
        [Export("setDelegate:")]
        void SetDelegate(POBAdRendererDelegate @delegate);

        // @required -(void)renderAdDescriptor:(id<POBAdDescriptor>)descriptor;
        [Abstract]
        [Export("renderAdDescriptor:")]
        void RenderAdDescriptor(POBAdDescriptor descriptor);
    }


    // @protocol POBInterstitialRendererDelegate
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBInterstitialRendererDelegate
    {
        // @required -(void)interstitialRendererDidRenderAd;
        [Abstract]
        [Export("interstitialRendererDidRenderAd")]
        void InterstitialRendererDidRenderAd();

        // @required -(void)interstitialRendererDidFailToRenderAdWithError:(NSError *)error;
        [Abstract]
        [Export("interstitialRendererDidFailToRenderAdWithError:")]
        void InterstitialRendererDidFailToRenderAdWithError(NSError error);

        // @required -(void)interstitialRendererDidClick;
        [Abstract]
        [Export("interstitialRendererDidClick")]
        void InterstitialRendererDidClick();

        // @required -(void)interstitialRendererWillLeaveApp;
        [Abstract]
        [Export("interstitialRendererWillLeaveApp")]
        void InterstitialRendererWillLeaveApp();

        // @required -(void)interstitialRendererWillPresentModal;
        [Abstract]
        [Export("interstitialRendererWillPresentModal")]
        void InterstitialRendererWillPresentModal();

        // @required -(void)interstitialRendererDidDismissModal;
        [Abstract]
        [Export("interstitialRendererDidDismissModal")]
        void InterstitialRendererDidDismissModal();

        // @required -(UIViewController *)viewControllerForPresentingModal;
        [Abstract]
        [Export("viewControllerForPresentingModal")]
        UIViewController ViewControllerForPresentingModal { get; }

        // @optional -(void)interstitialRendererDidUnloadMRAIDAd;
        [Export("interstitialRendererDidUnloadMRAIDAd")]
        void InterstitialRendererDidUnloadMRAIDAd();
    }

    // @interface POBApplicationInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface POBApplicationInfo
    {
        // @property (nonatomic, strong) NSString * _Nonnull domain;
        [Export("domain", ArgumentSemantic.Strong)]
        string Domain { get; set; }

        // @property (nonatomic, strong) NSURL * _Nonnull storeURL;
        [Export("storeURL", ArgumentSemantic.Strong)]
        NSUrl StoreURL { get; set; }

        // @property (assign, nonatomic) POBBOOL paid;
        [Export("paid")]
        nint Paid { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull categories;
        [Export("categories", ArgumentSemantic.Strong)]
        string Categories { get; set; }
    }

    // @interface POBAdSize : NSObject
    [BaseType(typeof(NSObject))]
    interface POBAdSize
    {
        // -(instancetype)initWithCGSize:(CGSize)size;
        [Export("initWithCGSize:")]
        IntPtr Constructor(CGSize size);

        // -(CGSize)cgSize;
        [Export("cgSize")]
        CGSize CgSize { get; }

        // -(BOOL)isZero;
        [Export("isZero")]
        bool IsZero { get; }

        // -(CGFloat)width;
        [Export("width")]
        nfloat Width { get; }

        // -(CGFloat)height;
        [Export("height")]
        nfloat Height { get; }
    }

    // @interface POBUserInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface POBUserInfo
    {
        // @property (nonatomic, strong) NSNumber * _Nonnull birthYear;
        [Export("birthYear", ArgumentSemantic.Strong)]
        NSNumber BirthYear { get; set; }

        // @property (assign, nonatomic) POBGender gender;
        [Export("gender", ArgumentSemantic.Assign)]
        POBGender Gender { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull metro;
        [Export("metro", ArgumentSemantic.Strong)]
        string Metro { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull zip;
        [Export("zip", ArgumentSemantic.Strong)]
        string Zip { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull city;
        [Export("city", ArgumentSemantic.Strong)]
        string City { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull region;
        [Export("region", ArgumentSemantic.Strong)]
        string Region { get; set; }

        // @property (nonatomic, strong) NSString * _Nonnull country;
        [Export("country", ArgumentSemantic.Strong)]
        string Country { get; set; }
    }

    // @interface OpenBidSDK : NSObject
    [BaseType(typeof(NSObject))]
    interface OpenBidSDK
    {
        // +(NSString *)version;
        [Static]
        [Export("version")]
        string Version { get; }

        // +(void)setLogLevel:(POBSDKLogLevel)logLevel;
        [Static]
        [Export("setLogLevel:")]
        void SetLogLevel(POBSDKLogLevel logLevel);

        // +(void)setGDPREnabled:(BOOL)gdprEnabled;
        [Static]
        [Export("setGDPREnabled:")]
        void SetGDPREnabled(bool gdprEnabled);

        // +(void)setGDPRConsent:(NSString *)gdprConsent;
        [Static]
        [Export("setGDPRConsent:")]
        void SetGDPRConsent(string gdprConsent);

        // +(void)allowLocationAccess:(BOOL)allow;
        [Static]
        [Export("allowLocationAccess:")]
        void AllowLocationAccess(bool allow);

        // +(void)useInternalBrowser:(BOOL)use;
        [Static]
        [Export("useInternalBrowser:")]
        void UseInternalBrowser(bool use);

        // +(void)setLocation:(CLLocation *)location source:(POBLocSource)source;
        [Static]
        [Export("setLocation:source:")]
        void SetLocation(CLLocation location, POBLocSource source);

        // +(void)setCoppaEnabled:(BOOL)enable;
        [Static]
        [Export("setCoppaEnabled:")]
        void SetCoppaEnabled(bool enable);

        // +(void)setSSLEnabled:(BOOL)enable;
        [Static]
        [Export("setSSLEnabled:")]
        void SetSSLEnabled(bool enable);

        // +(void)allowAdvertisingId:(BOOL)allow;
        [Static]
        [Export("allowAdvertisingId:")]
        void AllowAdvertisingId(bool allow);

        // +(void)setHashTypeForAdvertisingId:(POBHashType)hashType;
        [Static]
        [Export("setHashTypeForAdvertisingId:")]
        void SetHashTypeForAdvertisingId(POBHashType hashType);

        // +(void)setApplicationInfo:(POBApplicationInfo *)appInfo;
        [Static]
        [Export("setApplicationInfo:")]
        void SetApplicationInfo(POBApplicationInfo appInfo);

        // +(void)setUserInfo:(POBUserInfo *)userInfo;
        [Static]
        [Export("setUserInfo:")]
        void SetUserInfo(POBUserInfo userInfo);
    }

    // @protocol POBInterstitialRendering
    /*
      Check whether adding [Model] to this declaration is appropriate.
      [Model] is used to generate a C# class that implements this protocol,
      and might be useful for protocols that consumers are supposed to implement,
      since consumers can subclass the generated class instead of implementing
      the generated interface. If consumers are not supposed to implement this
      protocol, then [Model] is redundant and will generate code that will never
      be used.
    */
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBInterstitialRendering
    {
        // @required -(void)setDelegate:(id<POBInterstitialRendererDelegate>)delegate;
        [Abstract]
        [Export("setDelegate:")]
        void SetDelegate(POBInterstitialRendererDelegate @delegate);

        // @required -(void)renderAdDescriptor:(id<POBAdDescriptor>)descriptor;
        [Abstract]
        [Export("renderAdDescriptor:")]
        void RenderAdDescriptor(POBAdDescriptor descriptor);

        // @required -(void)showFromViewController:(UIViewController *)controller inOrientation:(UIInterfaceOrientation)loadTimeOrientation;
        [Abstract]
        [Export("showFromViewController:inOrientation:")]
        void ShowFromViewController(UIViewController controller, UIInterfaceOrientation loadTimeOrientation);
    }

    public interface IPOBBannerEvent { }

    // @protocol POBBannerEvent <NSObject>
    /*
      Check whether adding [Model] to this declaration is appropriate.
      [Model] is used to generate a C# class that implements this protocol,
      and might be useful for protocols that consumers are supposed to implement,
      since consumers can subclass the generated class instead of implementing
      the generated interface. If consumers are not supposed to implement this
      protocol, then [Model] is redundant and will generate code that will never
      be used.
    */
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBBannerEvent
    {
        // @required -(void)requestAdWithBid:(POBBid *)bid;
        [Abstract]
        [Export("requestAdWithBid:")]
        void RequestAdWithBid(POBBid bid);

        // @required -(void)setDelegate:(id<POBBannerEventDelegate>)delegate;
        [Abstract]
        [Export("setDelegate:")]
        void SetDelegate(IPOBBannerEventDelegate @delegate);

        // @required -(CGSize)adContentSize;
        [Abstract]
        [Export("adContentSize")]
        CGSize AdContentSize { get; }

        // @required -(NSArray<POBAdSize *> * _Nonnull)requestedAdSizes;
        [Abstract]
        [Export("requestedAdSizes")]
        POBAdSize[] RequestedAdSizes { get; }

        // @optional -(id)rendererForPartner:(NSString *)partner;
        [Export("rendererForPartner:")]
        NSObject RendererForPartner(string partner);
    }

    public interface IPOBBannerEventDelegate { }

    // @protocol POBBannerEventDelegate
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBBannerEventDelegate
    {
        // @required -(void)openBidPartnerDidWin;
        [Abstract]
        [Export("openBidPartnerDidWin")]
        void OpenBidPartnerDidWin();

        // @required -(void)adServerDidWin:(UIView *)view;
        [Abstract]
        [Export("adServerDidWin:")]
        void AdServerDidWin(UIView view);

        // @required -(void)failedWithError:(NSError *)error;
        [Abstract]
        [Export("failedWithError:")]
        void FailedWithError(NSError error);

        // @required -(void)willPresentModal;
        [Abstract]
        [Export("willPresentModal")]
        void WillPresentModal();

        // @required -(void)didDismissModal;
        [Abstract]
        [Export("didDismissModal")]
        void DidDismissModal();

        // @required -(void)willLeaveApp;
        [Abstract]
        [Export("willLeaveApp")]
        void WillLeaveApp();

        // @required -(UIViewController *)viewControllerForPresentingModal;
        [Abstract]
        [Export("viewControllerForPresentingModal")]
        UIViewController ViewControllerForPresentingModal { get; }
    }

    // @interface POBBannerImpression : POBImpression
    [BaseType(typeof(POBImpression))]
    interface POBBannerImpression
    {
        // -(instancetype _Nonnull)initWithImpressionId:(NSString * _Nonnull)impId adUnitId:(NSString * _Nonnull)adUnitId sizes:(NSArray * _Nonnull)sizes;
        [Export("initWithImpressionId:adUnitId:sizes:")]
        IntPtr Constructor(string impId, string adUnitId, NSObject[] sizes);

        // @property (readonly, nonatomic) NSArray<POBAdSize *> * _Nonnull sizes;
        [Export("sizes")]
        POBAdSize[] Sizes { get; }

        // @property (assign, nonatomic) POBAdPosition adPosition;
        [Export("adPosition", ArgumentSemantic.Assign)]
        POBAdPosition AdPosition { get; set; }
    }

    // @interface POBBannerView : UIView
    [BaseType(typeof(UIView))]
    interface POBBannerView
    {
        // -(instancetype)initWithPublisherId:(NSString * _Nonnull)publisherId profileId:(NSNumber * _Nonnull)profileId adUnitId:(NSString * _Nonnull)adUnitId eventHandler:(id<POBBannerEvent> _Nonnull)eventHandler;
        [Export("initWithPublisherId:profileId:adUnitId:eventHandler:")]
        IntPtr Constructor(string publisherId, NSNumber profileId, string adUnitId, IPOBBannerEvent eventHandler);

        // -(instancetype)initWithPublisherId:(NSString * _Nonnull)publisherId profileId:(NSNumber * _Nonnull)profileId adUnitId:(NSString * _Nonnull)adUnitId adSizes:(NSArray<POBAdSize *> * _Nonnull)adSizes;
        [Export("initWithPublisherId:profileId:adUnitId:adSizes:")]
        IntPtr Constructor(string publisherId, NSNumber profileId, string adUnitId, POBAdSize[] adSizes);

        // -(void)loadAd;
        [Export("loadAd")]
        void LoadAd();

        [Wrap("WeakDelegate")]
        [NullAllowed]
        POBBannerViewDelegate Delegate { get; set; }

        // @property (nonatomic, weak) id<POBBannerViewDelegate> _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (readonly, nonatomic) POBRequest * _Nonnull request;
        [Export("request")]
        POBRequest Request { get; }

        // @property (readonly, nonatomic) POBBannerImpression * _Nonnull impression;
        [Export("impression")]
        POBBannerImpression Impression { get; }

        // -(POBAdSize * _Nonnull)creativeSize;
        [Export("creativeSize")]
        POBAdSize CreativeSize { get; }
    }

    // @protocol POBBannerViewDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBBannerViewDelegate
    {
        // @required -(UIViewController * _Nonnull)bannerViewPresentationController;
        [Abstract]
        [Export("bannerViewPresentationController")]
        UIViewController BannerViewPresentationController { get; }

        // @optional -(void)bannerViewDidReceiveAd:(POBBannerView * _Nonnull)bannerView;
        [Export("bannerViewDidReceiveAd:")]
        void BannerViewDidReceiveAd(POBBannerView bannerView);

        // @optional -(void)bannerView:(POBBannerView * _Nonnull)bannerView didFailToReceiveAdWithError:(NSError * _Nullable)error;
        [Export("bannerView:didFailToReceiveAdWithError:")]
        void BannerView(POBBannerView bannerView, [NullAllowed] NSError error);

        // @optional -(void)bannerViewWillLeaveApplication:(POBBannerView * _Nonnull)bannerView;
        [Export("bannerViewWillLeaveApplication:")]
        void BannerViewWillLeaveApplication(POBBannerView bannerView);

        // @optional -(void)bannerViewWillPresentModal:(POBBannerView * _Nonnull)bannerView;
        [Export("bannerViewWillPresentModal:")]
        void BannerViewWillPresentModal(POBBannerView bannerView);

        // @optional -(void)bannerViewDidDismissModal:(POBBannerView * _Nonnull)bannerView;
        [Export("bannerViewDidDismissModal:")]
        void BannerViewDidDismissModal(POBBannerView bannerView);
    }

    // @interface POBBidSummary : NSObject
    [BaseType(typeof(NSObject))]
    interface POBBidSummary
    {
        // @property (readonly, nonatomic) NSString * bidder;
        [Export("bidder")]
        string Bidder { get; }

        // @property (readonly, nonatomic) float bid;
        [Export("bid")]
        float Bid { get; }

        // @property (readonly, nonatomic) CGSize size;
        [Export("size")]
        CGSize Size { get; }

        // @property (readonly, nonatomic) NSError * error;
        [Export("error")]
        NSError Error { get; }

        // -(instancetype)initWithSummary:(NSDictionary *)summaryDict;
        [Export("initWithSummary:")]
        IntPtr Constructor(NSDictionary summaryDict);
    }

    // @interface POBBid : NSObject
    [BaseType(typeof(NSObject))]
    interface POBBid
    {
        // -(instancetype)initWithBidDetails:(NSDictionary *)bidDetails andPartner:(NSString *)partner;
        [Export("initWithBidDetails:andPartner:")]
        IntPtr Constructor(NSDictionary bidDetails, string partner);

        // -(NSDictionary *)targetingInfo;
        [Export("targetingInfo")]
        NSDictionary TargetingInfo { get; }

        // @property (readonly, nonatomic) NSString * impressionId;
        [Export("impressionId")]
        string ImpressionId { get; }

        // @property (readonly, nonatomic) NSNumber * price;
        [Export("price")]
        NSNumber Price { get; }

        // @property (readonly, nonatomic) CGSize size;
        [Export("size")]
        CGSize Size { get; }

        // @property (readonly, nonatomic) NSNumber * status;
        [Export("status")]
        NSNumber Status { get; }

        // @property (readonly, nonatomic) NSString * creativeId;
        [Export("creativeId")]
        string CreativeId { get; }

        // @property (readonly, nonatomic) NSString * nurl;
        [Export("nurl")]
        string Nurl { get; }

        // @property (readonly, nonatomic) NSString * creativeTag;
        [Export("creativeTag")]
        string CreativeTag { get; }

        // @property (readonly, nonatomic) NSString * partner;
        [Export("partner")]
        string Partner { get; }

        // @property (readonly, nonatomic) NSString * dealId;
        [Export("dealId")]
        string DealId { get; }

        // @property (readonly, nonatomic) NSArray<POBBidSummary *> * summary;
        [Export("summary")]
        POBBidSummary[] Summary { get; }

        // @property (readonly, nonatomic) NSTimeInterval refreshInterval;
        [Export("refreshInterval")]
        double RefreshInterval { get; }
    }

    // @interface POBImpression : NSObject
    [BaseType(typeof(NSObject))]
    interface POBImpression
    {
        // -(instancetype)initWithImpressionId:(NSString *)impId adUnitId:(NSString *)adUnitId;
        [Export("initWithImpressionId:adUnitId:")]
        IntPtr Constructor(string impId, string adUnitId);

        // -(BOOL)isValid;
        [Export("isValid")]
        bool IsValid { get; }

        // @property (readonly, nonatomic) NSString * impressionId;
        [Export("impressionId")]
        string ImpressionId { get; }

        // @property (readonly, nonatomic) NSString * adUnitId;
        [Export("adUnitId")]
        string AdUnitId { get; }

        // @property (nonatomic, strong) NSString * pmZoneId;
        [Export("pmZoneId", ArgumentSemantic.Strong)]
        string PmZoneId { get; set; }

        // @property (nonatomic, strong) NSDictionary * customParams;
        [Export("customParams", ArgumentSemantic.Strong)]
        NSDictionary CustomParams { get; set; }
    }

    // @interface POBInterstitial : NSObject
    [BaseType(typeof(NSObject))]
    interface POBInterstitial
    {
        // -(instancetype)initWithPublisherId:(NSString * _Nonnull)publisherId profileId:(NSNumber * _Nonnull)profileId adUnitId:(NSString * _Nonnull)adUnitId eventHandler:(id<POBInterstitialEvent> _Nonnull)eventHandler;
        [Export("initWithPublisherId:profileId:adUnitId:eventHandler:")]
        IntPtr Constructor(string publisherId, NSNumber profileId, string adUnitId, POBInterstitialEvent eventHandler);

        // -(instancetype)initWithPublisherId:(NSString * _Nonnull)publisherId profileId:(NSNumber * _Nonnull)profileId adUnitId:(NSString * _Nonnull)adUnitId;
        [Export("initWithPublisherId:profileId:adUnitId:")]
        IntPtr Constructor(string publisherId, NSNumber profileId, string adUnitId);

        // -(void)loadAd;
        [Export("loadAd")]
        void LoadAd();

        // -(void)showFromViewController:(UIViewController * _Nonnull)controller;
        [Export("showFromViewController:")]
        void ShowFromViewController(UIViewController controller);

        [Wrap("WeakDelegate")]
        [NullAllowed]
        POBInterstitialDelegate Delegate { get; set; }

        // @property (nonatomic, weak) id<POBInterstitialDelegate> _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        // @property (readonly, nonatomic) POBRequest * _Nonnull request;
        [Export("request")]
        POBRequest Request { get; }

        // @property (readonly, nonatomic) POBInterstitialImpression * _Nonnull impression;
        [Export("impression")]
        POBInterstitialImpression Impression { get; }

        // @property (readonly, assign, nonatomic) BOOL isReady;
        [Export("isReady")]
        bool IsReady { get; }
    }

    // @protocol POBInterstitialDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBInterstitialDelegate
    {
        // @optional -(void)interstitialDidReceiveAd:(POBInterstitial * _Nonnull)interstitial;
        [Export("interstitialDidReceiveAd:")]
        void InterstitialDidReceiveAd(POBInterstitial interstitial);

        // @optional -(void)interstitial:(POBInterstitial * _Nonnull)interstitial didFailToReceiveAdWithError:(NSError * _Nullable)error;
        [Export("interstitial:didFailToReceiveAdWithError:")]
        void Interstitial(POBInterstitial interstitial, [NullAllowed] NSError error);

        // @optional -(void)interstitialWillPresentAd:(POBInterstitial * _Nonnull)interstitial;
        [Export("interstitialWillPresentAd:")]
        void InterstitialWillPresentAd(POBInterstitial interstitial);

        // @optional -(void)interstitialDidDismissAd:(POBInterstitial * _Nonnull)interstitial;
        [Export("interstitialDidDismissAd:")]
        void InterstitialDidDismissAd(POBInterstitial interstitial);

        // @optional -(void)interstitialWillLeaveApplication:(POBInterstitial * _Nonnull)interstitial;
        [Export("interstitialWillLeaveApplication:")]
        void InterstitialWillLeaveApplication(POBInterstitial interstitial);

        // @optional -(void)interstitialDidClickAd:(POBInterstitial * _Nonnull)interstitial;
        [Export("interstitialDidClickAd:")]
        void InterstitialDidClickAd(POBInterstitial interstitial);
    }

    public interface IPOBInterstitialEventDelegate { }

    // @protocol POBInterstitialEventDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBInterstitialEventDelegate
    {
        // @required -(NSDictionary *)customData;
        [Abstract]
        [Export("customData")]
        NSDictionary CustomData { get; }

        // @required -(void)openBidPartnerDidWin;
        [Abstract]
        [Export("openBidPartnerDidWin")]
        void OpenBidPartnerDidWin();

        // @required -(void)adServerDidWin;
        [Abstract]
        [Export("adServerDidWin")]
        void AdServerDidWin();

        // @required -(void)failedWithError:(NSError *)error;
        [Abstract]
        [Export("failedWithError:")]
        void FailedWithError(NSError error);

        // @required -(void)willPresentAd;
        [Abstract]
        [Export("willPresentAd")]
        void WillPresentAd();

        // @required -(void)didDismissAd;
        [Abstract]
        [Export("didDismissAd")]
        void DidDismissAd();

        // @required -(void)willLeaveApp;
        [Abstract]
        [Export("willLeaveApp")]
        void WillLeaveApp();

        // @required -(void)didClickAd;
        [Abstract]
        [Export("didClickAd")]
        void DidClickAd();
    }

    // @protocol POBInterstitialEvent <NSObject>
    /*
      Check whether adding [Model] to this declaration is appropriate.
      [Model] is used to generate a C# class that implements this protocol,
      and might be useful for protocols that consumers are supposed to implement,
      since consumers can subclass the generated class instead of implementing
      the generated interface. If consumers are not supposed to implement this
      protocol, then [Model] is redundant and will generate code that will never
      be used.
    */
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface POBInterstitialEvent
    {
        // @required -(void)requestAdWithBid:(POBBid *)bid;
        [Abstract]
        [Export("requestAdWithBid:")]
        void RequestAdWithBid(POBBid bid);

        // @required -(void)setDelegate:(id<POBInterstitialEventDelegate>)delegate;
        [Abstract]
        [Export("setDelegate:")]
        void SetDelegate(IPOBInterstitialEventDelegate @delegate);

        // @required -(void)showFromViewController:(UIViewController * _Nonnull)controller;
        [Abstract]
        [Export("showFromViewController:")]
        void ShowFromViewController(UIViewController controller);

        // @optional -(id)rendererForPartner:(NSString *)partner;
        [Export("rendererForPartner:")]
        NSObject RendererForPartner(string partner);
    }

    // @interface POBInterstitialImpression : POBImpression
    [BaseType(typeof(POBImpression))]
    interface POBInterstitialImpression
    {
    }

    // @interface POBRequest : NSObject
    [BaseType(typeof(NSObject))]
    interface POBRequest
    {
        // @property (nonatomic, strong) NSNumber * versionId;
        [Export("versionId", ArgumentSemantic.Strong)]
        NSNumber VersionId { get; set; }

        // @property (assign, nonatomic) BOOL bidSummaryEnabled;
        [Export("bidSummaryEnabled")]
        bool BidSummaryEnabled { get; set; }

        // @property (assign, nonatomic) BOOL debug;
        [Export("debug")]
        bool Debug { get; set; }

        // @property (nonatomic, strong) NSString * userKeywords;
        [Export("userKeywords", ArgumentSemantic.Strong)]
        string UserKeywords { get; set; }

        // @property (readonly, strong) NSString * publisherId;
        [Export("publisherId", ArgumentSemantic.Strong)]
        string PublisherId { get; }

        // @property (readonly, strong) NSNumber * profileId;
        [Export("profileId", ArgumentSemantic.Strong)]
        NSNumber ProfileId { get; }

        // @property (readonly, nonatomic) NSArray<POBImpression *> * impressions;
        [Export("impressions")]
        POBImpression[] Impressions { get; }

        // @property (nonatomic, strong) NSString * adServerURL;
        [Export("adServerURL", ArgumentSemantic.Strong)]
        string AdServerURL { get; set; }

        // @property (getter = isTestModeEnabled, assign, nonatomic) BOOL testModeEnabled;
        [Export("testModeEnabled")]
        bool TestModeEnabled { [Bind("isTestModeEnabled")] get; set; }

        // @property (assign, nonatomic) NSTimeInterval networkTimeout;
        [Export("networkTimeout")]
        double NetworkTimeout { get; set; }

        // -(instancetype)initWithPublisherId:(NSString *)pubId profileId:(NSNumber *)profileId impressions:(NSArray<POBImpression *> *)impressions;
        [Export("initWithPublisherId:profileId:impressions:")]
        IntPtr Constructor(string pubId, NSNumber profileId, POBImpression[] impressions);
    }

    // @interface POBRenderer : NSObject
    [BaseType(typeof(NSObject))]
    interface POBRenderer
    {
        // +(id)bannerRenderer;
        [Static]
        [Export("bannerRenderer")]
        NSObject BannerRenderer { get; }

        // +(id)interstitialRenderer;
        [Static]
        [Export("interstitialRenderer")]
        NSObject InterstitialRenderer { get; }
    }
}

